<?php
/**
 * @file
 * Contains install and update functions for custom_keywords.
 */

/**
 * Implements hook_install().
 */
function custom_keywords_install() {
  //build initial sets
  custom_keywords_cleanup_sets();
}

/**
 * Implements hook_schema().
 */
function custom_keywords_schema() {
  $schema = array();

  //keywords table
  $schema['custom_keywords'] = array(
     'description' => t('Custom keywords that are defined.'),
     'fields' => array(
        'kid' => array(
          'description' => t('The unique id for a keyword'),
          'type'        => 'serial',
          'not null'    => TRUE,
        ),
        'name' => array(
          'description' => t('A unique machine name to identify the keyword'),
          'type'        => 'varchar',
          'length'      => '50',
          'not null'    => TRUE,
        ),
        'description' => array(
          'description' => t('A description of the keyword'),
          'type'        => 'varchar',
          'length'      => '255',
        ),
       'default_value' => array(
          'description' => t('A default value for the keyword'),
          'type'        => 'varchar',
          'length'      => '255',
        ),
      ),
     'primary key' => array('kid'),
     'unique keys' => array(
        'name' => array('name'),
      ),
  );

  //keyword sets table
  $schema['custom_keywords_sets'] = array(
    'description' => t('Saves groupsings for keywords'),
    'fields' => array(
       'sid' => array(
          'description' => t('The unique id for a set'),
          'type'        => 'serial',
          'not null'    => TRUE,
        ),
      'name' => array(
          'description' => t('A unique machine name to identify the set'),
          'type'        => 'varchar',
          'length'      => '50',
          'not null'    => TRUE,
        ),
      'enabled' => array(
        'description' => t('A flag to determine if the set is active'),
        'type'        => 'int',
        'not null'    => TRUE,
        'defualt'    => 1,
      ),
    ),
    'primary key' => array('sid'),
    'unique keys' => array(
        'name'  => array('name'),
    ),
  );

  //set default values table
  $schema['custom_keywords_set_values'] = array(
    'description' => t('Unique keyword values for a sets.'),
    'fields' => array(
      'set_name'  => array(
        'description' => t('The unique machine name of a set.'),
        'type'        => 'varchar',
        'length'      => '50',
      ),
      'keyword' => array(
        'description' => t('The unique machine name of a keyword.'),
        'type'        => 'varchar',
        'length'      => '50',
      ),
      'custom_value'  => array(
        'description' => t('The unique keyword value for this set.'),
        'type'        => 'varchar',
        'length'      => '255',
      ),
    ),
    'unique keys' => array(
      'set_keyword' => array('set_name', 'keyword'),
    ),
    'foreign_keys' => array(
      'set_name' => array('custom_keywords_sets' => 'name'),
      'keyword' => array('custom_keywords' => 'name'),
    ),
  );

  //entity default values table
  $schema['custom_keywords_entity_values'] = array(
    'description' => t('Unique keyword values for an entity.'),
    'fields' => array(
      'type'  => array(
        'description' => t('The unique machine name of an entity type'),
        'type'        => 'varchar',
        'length'      => '50',
      ),
      'eid'   => array(
        'description' => t('The unique id of an entity'),
        'type'        => 'int',
        'not null'    => TRUE,
        'unsigned'    => TRUE,
      ),
      'vid'   => array(
        'description' => t('The revision id of the entity'),
        'type'        => 'int',
        'not null'    => TRUE,
        'unsigned'    => TRUE,
        'default'     => 1,
      ),
      'language' => array(
        'description' => 'The language of the entity',
        'type'        => 'varchar',
        'length'      => 32,
        'not null'    => TRUE,
        'default'     => '',
      ),
      'keyword' => array(
        'description' => t('The unique machine name of a keyword.'),
        'type'        => 'varchar',
        'length'      => '50',
      ),
      'custom_value'  => array(
        'description' => t('The unique keyword value for this set.'),
        'type'        => 'varchar',
        'length'      => '255',
      ),
    ),
    'primary key' => array('type', 'eid', 'vid', 'keyword', 'language'),
    'unique keys' => array(
      'set_keyword' =>  array('type', 'eid', 'vid', 'keyword', 'language'),
    ),
    'foreign_keys' => array(
      'keyword' => array('custom_keywords' => 'name'),
    ),
  );

  //cache table
  $schema['cache_custom_keywords'] = drupal_get_schema_unprocessed('system', 'cache');
  $schema['cache_custom_keywords']['description'] = t('Cache table for the generated custom keywords.');

  return $schema;
}
