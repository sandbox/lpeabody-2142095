<?php
/**
 * @file
 *
 * Administrator form code and helper functions for Custom Keywords module.
 */

/**
 * Display admin page for managing sets
 * @return string renders HTML
 */
function custom_keywords_admin_sets() {
  //get all sets as a nested array
  if ($sets = custom_keywords_get_nested_sets()) {
    $fieldsets = array();
    //foreach set create a form fieldset
    foreach ($sets as $name => $set) {
     _custom_keywords_admin_set_fieldsets($fieldsets, $name, $set);
    }
    //return rendered form elements.
    return drupal_render($fieldsets);
  }
  else {
    return t('No Valid sets detected. Run cron to auto detect valid entity sets');
  }
}

/**
 * recursive function for generating nested fieldsets of sets
 * @param  array  $form  form array
 * @param  string  $name  current set name
 * @param  array  $set   current set data
 * @param  integer $level nesting level
 * @return none
 */
function _custom_keywords_admin_set_fieldsets(&$form, $name, $set, $level=1) {
  //build title without parent information
  $title = $name;
  if (strpos($name, ':')) {
    $title = substr($name, strrpos($name, ':')+1);
  }

  //create fieldset for set
  $form[$name] = array(
    '#type'         => 'fieldset',
    '#collapsible'  => TRUE,
    '#collapsed'    => ($level > 1),
    '#title'        => $title,
  );

  //add available actions
  $form[$name]['actions'] = array(
    '#type'         => 'markup',
    '#markup'       => l(t('Manage Defaults'), 'admin/config/content/custom-keywords/sets/' . $name . '/keywords')
                      . ' | ' . l(($set['enabled'] ? 'Dis':'En') . 'able', 'admin/config/content/custom-keywords/sets/' . $name . '/' . ($set['enabled'] ? 'dis':'en') . 'able'),
  );

  //if set has nested children recursivly add them.
  if (isset($set['children'])) {
    foreach ($set['children'] as $child_name => $child_set) {
      _custom_keywords_admin_set_fieldsets($form[$name], $child_name, $child_set, $level+1);
    }
  }
}

/**
 * create form for managing keyowrd defaults for a set
 * @param  string $form_id    form id
 * @param  array $form_state form state array
 * @param  string $set_name   set name
 * @return array              form array
 */
function custom_keywords_admin_set_keywords($form_id, &$form_state, $set_name) {
  $form = array();
    //build title without parent information
  $title = $set_name;
  if (strpos($set_name, ':')) {
    $title = substr($set_name, strrpos($set_name, ':')+1);
  }
  //get keywords for set
  if ($set = custom_keywords_get_sets($set_name)) {
    $form['#set_name']  = $set_name;
    $form['#set']       = $set;
    //need to get keywords
    $keywords = custom_keywords_load_set_keywords($set_name);
    //keywords exists?
    if (sizeof($keywords) > 0) {
      $form['#keywords'] = $keywords;

      $form['keywords'] = array(
        '#type'         => 'fieldset',
        '#title'        => t($title . ' Keyword Defaults'),
        '#description'  => t('Set Default values for this set. All entities/child sets of this set will inherit these values.'),
      );
      //build field for each keyword
      foreach ($keywords as $keyword => $data) {
        $form['keywords'][$keyword] = array(
          '#type'           => 'textfield',
          '#title'          => '%' . $keyword,
          '#default_value'  => $data['value'],
          '#description'    => $data['description'] . '. Accepts Tokens',
        );
      }

      // Show the list of available tokens. for default value settings
      $form['tokens'] = array(
        '#theme'        => 'token_tree',
        '#token_types'  => token_get_entity_mapping(),
        '#dialog'       => TRUE,
      );

      $form['actions']['#type'] = 'actions';

      $form['actions']['submit'] = array(
        '#type'   => 'submit',
        '#value'  => t('Save Defaults'),
      );
    }
    else {
      //no keywords available.
      $form['error'] = array(
        '#type' => 'markup',
        '#markup' => t('No custom ctool keywords have been created. !create',
          array('!create' => l(t('Create Keywords'), 'admin/config/content/custom-keywords/keywords/add'))
        ),
      );
    }
  }
  else{
    //invalid set specified
    drupal_set_message(t('Invalid Set specified'), 'error', FALSE);
    drupal_goto('admin/config/content/custom-keywords/sets');
  }

  return $form;
}

/**
 * Submit handler for the set keyword form.
 * @param  array $form
 * @param  array $form_state
 * @return none
 */
function custom_keywords_admin_set_keywords_submit($form, &$form_state) {
  $set_name = $form['#set_name'];
  $keywords = $form['#keywords'];
  $keyword_values = array();

  //retrieve keyword values
  foreach ($keywords as $name => $data) {
    if (isset($form_state['values'][$name])) {
      $value = filter_xss(trim($form_state['values'][$name]));
      if (strlen($value) > 0) {
        $keyword_values[$name] = $value;
      }
    }
  }

  //attempt to save keywordd values.
  if (custom_keywords_save_set_keywords($set_name, $keyword_values)) {
    drupal_set_message(t('Keyword defaults updated.'), 'status', FALSE);
    drupal_goto('admin/config/content/custom-keywords/sets');
  }
}

/**
 * function to enable or disable a set
 * @param  string $set_name set machine name
 * @param  string $status   enable or disable
 * @return none
 */
function custom_keywords_admin_toggle_set($set_name, $status) {
  //passed a valid set?
  if ($set = custom_keywords_get_sets($set_name)) {
    //change status based on passed value
    $set['enabled'] = ($status == 'enabled') ? 1:0;

    //attempt to update the set
    if (custom_keywords_save_set($set_name, $set)) {
      drupal_set_message(t('Set ' . $status . 'ed'), 'status', FALSE);
    }
    else {
      drupal_set_message(t('Unable to update set'), 'error', FALSE);
    }
  }
  else {//invalid set specified
    drupal_set_message(t('Invalid Set specified'), 'error', FALSE);
  }

  drupal_goto('admin/config/content/custom-keywords/sets');
}

/**
 * Page callback for the keyword add form.
 * @param  array $form
 * @param  array $form_state
 * @param  array $keyword_object
 * @return array
 */
function custom_keywords_admin_add_keyword($form, &$form_state, $keyword_object = NULL) {
  if ($keyword_object) {
    $form['#keyword'] = $keyword_object;
  }

  $form['name'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Name'),
    '#required'       => TRUE,
    '#default_value'  => isset($keyword_object) ? $keyword_object['name'] : '',
    '#disabled'       => isset($keyword_object) ? TRUE : FALSE,
    '#description'    => t('An alphanumeric unique keyword name.'),
  );

  $form['description'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Description'),
    '#default_value'  => isset($keyword_object) ? $keyword_object['description'] : '',
    '#description'    => t('A description about the purpose of this keyword.'),
  );

  $form['value'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Default Value'),
    '#default_value'  => isset($keyword_object) ? $keyword_object['value'] : '',
    '#description'    => t('The default global value for this keyword. Accepts tokens.'),
  );

  // Show the list of available tokens. for default value settings
  $form['tokens'] = array(
    '#theme'        => 'token_tree',
    '#token_types'  => token_get_entity_mapping(),
    '#dialog'       => TRUE,
  );

  $form['actions']['#type'] = 'actions';

  $form['actions']['submit'] = array(
    '#type'  => 'submit',
    '#value' => (isset($keyword_object) ? 'Edit' : 'Add' ) . ' Keyword',
  );

  $form['actions']['cancel'] = array(
    '#type'   => 'markup',
    '#markup' => l(t('Cancel'), 'admin/config/content/custom-keywords/keywords'),
  );

  return $form;
}

/**
 * Validation handler for the keyword add form.
 * @param  array $form
 * @param  array $form_state
 * @return none
 */
function custom_keywords_admin_add_keyword_validate($form, &$form_state) {
  $name = $form_state['values']['name'];
  if (!preg_match('/^[a-z0-9_]+$/', $name)) {
    form_set_error('name', t('Keyword name must use lower case letters, numbers, and underscores.'));
  }
}

/**
 * Submit handler for the keyword add form.
 * @param  array $form
 * @param  array $form_state
 * @return none
 */
function custom_keywords_admin_add_keyword_submit($form, &$form_state) {
  $name = $form_state['values']['name'];
  $value = $form_state['values']['value'];
  $description = $form_state['values']['description'];
  $result = custom_keywords_save_keyword($name, array('value' => $value, 'description' => $description));
  if ($result) {
    drupal_set_message(t('Successfully added new keyword.'), 'status');
  }
  else {
    drupal_set_message(t('New keyword could not be saved.  Check your logs for errors.'), 'error');
  }
}

/**
 * Page callback for listing all keywords.
 */
function custom_keywords_admin_keywords() {
  $keywords = custom_keywords_load_keywords();
  $header = array('Keyword', 'Value', 'Description', 'Actions');
  $rows = array();

  foreach ($keywords as $key => $value) {
    $links = array(
      'edit' => array(
        'title' => 'Edit',
        'href' => "admin/config/content/custom-keywords/keywords/{$key}/edit"
      ),
      'delete' => array(
        'title' => 'Delete',
        'href' => "admin/config/content/custom-keywords/keywords/{$key}/delete"
      ),
    );

    $rows[] = array(
      $key, $value['value'], $value['description'], theme('links', array(
        'links' => $links, 'attributes' => array(
          'class' => array('links', 'inline')
          )
        )
      )
    );
  }

  return theme('table', array('header' => $header, 'rows' => $rows));
}

/**
 * Utility function for extracting the bundle/entity type name information
 * given a set's machine name.
 *
 * @param $set_name
 *   The set's machine name.
 *
 * @return
 *   Either an entity type name or bundle name.
 */
function _custom_keywords_get_set_information($set_name) {
  $terms = explode(':', $set_name);
  if (count($terms) == 1) {
    // just entity type is specified
    $info = entity_get_info($terms[0]);
    return $info['label'];
  }
  elseif (count($terms) == 2) {
    // entity type and bundle are specified
    $bundles = field_info_bundles($terms[0]);
    // $terms[1] will be the bundle machine name
    return $bundles[$terms[1]]['label'];
  }

  // unexpected results so just return FALSE
  return FALSE;
}

/**
 * Page callback for the keyword delete form.
 * @param  array $form
 * @param  array $form_state
 * @param  array $keyword_object
 * @return none
 */
function custom_keywords_admin_delete_keyword($form, &$form_state, $keyword_object) {
  $form['#keyword'] = $keyword_object;
  $form['keyword_name'] = array('#type' => 'value', '#value' => $keyword_object['name']);
  return confirm_form($form,
    t('Are you sure you want to delete %title?', array('%title' => $keyword_object['name'])),
    'admin/config/content/custom-keywords/keywords',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Submit handler for the keyword delete form.
 * @param  array $form
 * @param  array $form_state
 * @return none
 */
function custom_keywords_admin_delete_keyword_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $keyword = $form_state['values']['keyword_name'];
    if (custom_keywords_delete_keywords(array($keyword))) {
      drupal_set_message(t('Deleted keyword <em>!keyword</em>.', array('!keyword' => check_plain($keyword))), 'status');
    }
    else {
      drupal_set_message(t('Could not delete the keyword from the database.'), 'error');
    }
  }

  $form_state['redirect'] = 'admin/config/content/custom-keywords/keywords';
}
