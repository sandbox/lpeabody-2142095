<?php
/**
 * @file
 *
 * Contains context plugin code for Custom Keywords module.
 */

// define ctools plugin settings
$plugin = array(
  'title' => t('Custom Keywords'),
  'description' => t('A context that adds custom keywords with default values.'),
  'context' => 'ctools_context_create_custom_keywords',  // func to create context
  'context name' => 'custom_keywords',
  'keyword' => 'custom_keywords',
  'convert list' => 'ctools_context_custom_keywords_convert_list',
  'convert' => 'ctools_context_custom_keywords_convert',
);

/**
 * Create a context from manual configuration.
 */
function ctools_context_create_custom_keywords($empty, $data = NULL, $conf = FALSE) {
  $context = new ctools_context('custom_keywords');
  $context->plugin = 'custom_keywords';

  return $context;
}

function ctools_context_custom_keywords_convert_list() {
  // token value = label value
  $keywords = custom_keywords_load_keywords();
  foreach ($keywords as $key => $value) {
    $list[$key] = $value['description'];
  }
  return $list;
}

function ctools_context_custom_keywords_convert($context, $keyword) {
  $entity = menu_get_object();
  if (isset($entity) && property_exists($entity, 'nid')) {
    $entity_type = 'node';
  }
  elseif (isset($entity) && property_exists($entity, 'tid')) {
    $entity_type = 'taxonomy_term';
  }
  elseif (isset($entity) && property_exists($entity, 'uid')) {
    $entity_type = 'user';
  }
  else {
    $entity_type = NULL;
  }

  $current_language = LANGUAGE_NONE;
  if (isset($entity)) {
    $current_language = $entity->language;
  }
  elseif (function_exists('i18n_language')) {
    $current_language = i18n_language()->language;
  }

  list($id, $vid, $bundle) = isset($entity) ? entity_extract_ids($entity_type, $entity) : array(NULL, NULL, NULL);
  $keywords = custom_keywords_load_entity_keywords($entity_type, $bundle, $id, $vid, $current_language);

  //load parent/global defaults
  custom_keywords_load_entity_keyword_defaults($keywords, $entity_type, $bundle);
  // Replace any tokens.
  $keyword_value = token_replace($keywords[$keyword]['value']);

  return $keyword_value;
}
