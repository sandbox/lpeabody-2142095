<?php
/**
 * @file
 * Contains module code for Custom Keywords.
 */

/**
 * Implements hook_menu().
 */
function custom_keywords_menu() {

  $items['admin/config/content/custom-keywords'] = array(
    'title'             => 'Custom Keywords',
    'description'       => 'Manage and configure custom ctool keywords.',
    // TODO: convert this to a form so mass operations can be applied
    'page callback'     => 'custom_keywords_admin_keywords',
    'access arguments'  => array('administer custom keywords'),
    'type'              => MENU_NORMAL_ITEM,
    'file'              => 'custom_keywords.admin.inc',
  );

  $items['admin/config/content/custom-keywords/keywords'] = array(
    'title'             => 'Custom Keywords',
    'description'       => 'Manage and configure custom ctool keywords.',
    'type'              => MENU_DEFAULT_LOCAL_TASK,
  );

  $items['admin/config/content/custom-keywords/keywords/add'] = array(
    'title'             => 'Add Custom Keyword',
    'description'       => 'Create a new custom ctool keyword.',
    'page callback'     => 'drupal_get_form',
    'page arguments'    => array('custom_keywords_admin_add_keyword'),
    'access arguments'  => array('edit custom keywords'),
    'type'              => MENU_LOCAL_ACTION,
    'file'              => 'custom_keywords.admin.inc',
  );

  $items['admin/config/content/custom-keywords/keywords/%custom_keywords/edit'] = array(
    'title'             => 'Edit Custom Keyword',
    'description'       => 'Edit an existing custom ctool keyword.',
    'page callback'     => 'drupal_get_form',
    'page arguments'    => array('custom_keywords_admin_add_keyword', 5),
    'access arguments'  => array('edit custom keywords'),
    'type'              => MENU_CALLBACK,
    'file'              => 'custom_keywords.admin.inc',
  );

  $items['admin/config/content/custom-keywords/keywords/%custom_keywords/delete'] = array(
    'title'             => 'Delete Custom Keyword',
    'description'       => 'Delete an existing custom ctool keyword.',
    'page callback'     => 'drupal_get_form',
    'page arguments'    => array('custom_keywords_admin_delete_keyword', 5),
    'access arguments'  => array('edit custom keywords'),
    'type'              => MENU_CALLBACK,
    'file'              => 'custom_keywords.admin.inc',
  );

  $items['admin/config/content/custom-keywords/sets'] = array(
    'title'             => 'Keyword Defaults',
    'description'       => 'Manage Sets of Keyword Defaults',
    'page callback'     => 'custom_keywords_admin_sets',
    'access arguments'  => array('administer custom keywords'),
    'type'              => MENU_LOCAL_TASK,
    'tab_parent'        => 'admin/config/content/custom-keywords',
    'file'              => 'custom_keywords.admin.inc',
  );

  $items['admin/config/content/custom-keywords/sets/%/keywords'] = array(
    'title'             => 'Edit Set keywords',
    'description'       => 'Edit default keyword values for a set.',
    'page callback'     => 'drupal_get_form',
    'page arguments'    => array('custom_keywords_admin_set_keywords', 5),
    'access arguments'  => array('edit custom keywords'),
    'type'              => MENU_CALLBACK,
    'file'              => 'custom_keywords.admin.inc',
  );

  $items['admin/config/content/custom-keywords/sets/%/enable'] = array(
    'title'             => 'Enable Keyword Set',
    'description'       => 'Enable a keyword set.',
    'page callback'     => 'custom_keywords_admin_toggle_set',
    'page arguments'    => array(5, 6),
    'access arguments'  => array('administer custom keywords'),
    'type'              => MENU_CALLBACK,
    'file'              => 'custom_keywords.admin.inc',
  );

  $items['admin/config/content/custom-keywords/sets/%/disable'] = array(
    'title'             => 'Disable Keyword Set',
    'description'       => 'Disable a keyword set.',
    'page callback'     => 'custom_keywords_admin_toggle_set',
    'page arguments'    => array(5, 6),
    'access arguments'  => array('administer custom keywords'),
    'type'              => MENU_CALLBACK,
    'file'              => 'custom_keywords.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_cron().
 */
function custom_keywords_cron() {
  custom_keywords_cleanup_sets();
}

/**
 * Implements hook_ctools_plugin_directory().
 */
function custom_keywords_ctools_plugin_directory($owner, $plugin_type) {
  if ($owner == 'ctools') {
    return 'plugins/' . $plugin_type;
  }
}

/**
 * Implements hook_permission().
 */
function custom_keywords_permission() {
  $permissions['administer custom keywords'] = array(
    'title'           => 'Administer custom keywords.',
    'restrict access' => TRUE,
    'description'     => 'Control the main settings pages and modify per-object custom keywords.',
  );
  $permissions['edit custom keywords'] = array(
    'title'       => 'Edit custom keywords',
    'description' => 'Modify custom keywords on individual entity records (nodes, terms, users, etc).',
  );
  return $permissions;
}

/**
 * Implements hook_entity_load().
 */
function custom_keywords_entity_load($entities, $type) {

  foreach ($entities as $entity) {
    list($id, $vid, $bundle) = entity_extract_ids($type, $entity);
    $language = custom_keywords_entity_get_language($type, $entity);
    // Load only the custom keyword values for this entity.
    $entity->custom_keywords = custom_keywords_load_entity_keywords($type, $bundle, $id, $vid, $language);
  }
}

/**
 * Implements hook_entity_insert().
 */
function custom_keywords_entity_insert($entity, $type) {
  if (isset($entity->custom_keywords)) {
    custom_keywords_save_entity_keywords($entity, $type);
  }
}

/**
 * Implements hook_entity_update().
 */
function custom_keywords_entity_update($entity, $type) {
  if (isset($entity->custom_keywords)) {
    custom_keywords_save_entity_keywords($entity, $type);
  }
}

/**
 * Implements hook_entity_delete().
 */
function custom_keywords_entity_delete($entity, $type) {
  //get the entity id
  list($id) = entity_extract_ids($type, $entity);

  //delete any custom keywords for this entity
  db_delete('custom_keywords_entity_values')
    ->condition('type', $type)
    ->condition('eid', $id)
    ->execute();
}


/**
 * Implements hook_node_revision_delete().
 */
function custom_keywords_node_revision_delete($node) {
  //delete the keywords for this revision
  list($id, $vid, $bundle) = entity_extract_ids('node', $node);

  db_delete('custom_keywords_entity_values')
    ->condition('type', 'node')
    ->condition('eid', $id)
    ->condition('vid', $vid)
    ->execute();
}

/**
 * Implements hook_field_attach_form().
 */
function custom_keywords_field_attach_form($type, $entity, &$form, &$form_state, $langcode) {
   // Entity_Translation will trigger this hook again, skip it.
  if (!empty($form_state['entity_translation']['is_translation'])) {
    return;
  }

  list($eid, $vid, $bundle) = entity_extract_ids($type, $entity);

  // Grab the keywords to display if there are any
  if (!empty($entity->custom_keywords)) {
    // Identify the language to use with this entity.
    $language = custom_keywords_entity_get_language($type, $entity);
    // If this is a new translation using Entity Translation, load the keywords
    // from the entity's original language.
    if (module_exists('entity_translation') && empty($form['#entity_translation_source_form']) && ($handler = entity_translation_entity_form_get_handler($form, $form_state)) && custom_keywords_get_entity_values($type, $eid, $vid, $handler->getSourceLanguage())) {
      $keywords = custom_keywords_load_entity_keywords($type, $bundle, $eid, $vid, $handler->getSourceLanguage());
    }
    // Determine from where we should get the keywords.
    elseif (custom_keywords_get_entity_values($type, $eid, $vid, $langcode)) {
      // Set the tags to the translation set matching that of the form.
      $keywords = custom_keywords_load_entity_keywords($type, $bundle, $eid, $vid, $langcode);
    }
    // There is no translation for this entity's tags in the current
    // language. Instead, display keywords in the language of the entity, the
    // source language of translations. The will provide translators with the
    // original text to translate.
    elseif (custom_keywords_get_entity_values($type, $eid, $vid, $language)) {
      $keywords = custom_keywords_load_entity_keywords($type, $bundle, $eid, $vid, $language);
    }
    // This is a preview so set the keywords to the raw submission data.  No
    // language has been set.
    else {
      $keywords = $entity->custom_keywords;
    }
  }
  else {
    $keywords = array();
  }
  //load structure of all custom keywords with default values
  custom_keywords_load_entity_keyword_defaults($keywords, $type, $bundle);

  $options['token types'] = array(token_get_entity_mapping('entity', $type));
  $options['context']     = $type;

  $form['#custom_keywords'] = array(
    'keywords' => $keywords,
    'options'  => $options,
  );
}

/**
 * Implements hook_form_alter().
 *
 * @todo Remove this when http://drupal.org/node/1284642 is fixed in core.
 */
function custom_keywords_form_alter(&$form, $form_state, $form_id) {
  if (!empty($form['#custom_keywords']) && !isset($form['custom_keywords'])) {
    extract($form['#custom_keywords']);
    custom_keywords_keywords_form($form, $keywords, $options);
    unset($form['#custom_keywords']);
  }
}

/**
 * Implements hook_field_extra_fields().
 */
function custom_keywords_field_extra_fields() {
  $extra = array();
  foreach (entity_get_info() as $entity_type => $entity_info) {
    foreach (array_keys($entity_info['bundles']) as $bundle) {
      $extra[$entity_type][$bundle]['form']['custom_keywords'] = array(
        'label'       => t('Custom Keywords'),
        'description' => 'Custom Keywords module form elements.',
        'weight'      => 40,
      );
    }
  }
  return $extra;
}

// ===== CUSTOM FUNCTIONS

/**
 * Adds form elements for custom keywords to a form.
 *
 * @param  array  $form     form array
 * @param  array  $keywords keywords array
 * @param  array  $options  additionl options array
 * @return none
 */
function custom_keywords_keywords_form(array &$form, array $keywords = array(), array $options = array()) {

  if (empty($keywords)) {
    return;
  }

  $form['custom_keywords'] = array(
    '#type'         => 'fieldset',
    '#title'        => t('Custom Keywords'),
    '#multilingual' => TRUE,
    '#collapsible'  => TRUE,
    '#collapsed'    => TRUE,
    '#tree'         => TRUE,
    '#access'       => user_access('edit custom keywords') || user_access('administer custom keywords'),
    '#weight'       => 40,
    '#attributes'   => array(
      'class' => array('custom_keywords-form'),
    ),
  );

  // Only support vertical tabs if there is a vertical tab element.
  foreach (element_children($form) as $key) {
    if (isset($form[$key]['#type']) && $form[$key]['#type'] == 'vertical_tabs') {
      $form['custom_keywords']['#group'] = $key;
      break;
    }
  }

  //build the form for each custom keyword
  foreach ($keywords as $name => $keyword) {
    $form['custom_keywords'][$name] = array(
      '#type'           => 'textfield',
      '#title'          => check_plain('%' . $name),
      '#description'    => filter_xss($keyword['description']),
      '#default_value'  => $keyword['value'],
    );
  }

  // Show the list of available tokens.
  $form['custom_keywords']['tokens'] = array(
    '#theme'        => 'token_tree',
    '#token_types'  => $options['token types'],
    '#weight'       => 999,
    '#dialog'       => TRUE,
  );
}

/**
 * save or update a keyword
 * @param  string $name keyword machine name
 * @param  array $data array of keyword data
 * @return boolean       boolean TRUE or FALSE
 */
function custom_keywords_save_keyword($name, $data) {
  $default_data = array(
    'description'   => '',
    'value'         => '',
  );
  //add default fields.
  $data = array_merge($default_data, $data);

  //run merge query to insert/update
  $merge = db_merge('custom_keywords')
    ->key(array(
        'name' => $name,
    ))
    ->fields(array(
      'description'   => trim($data['description']),
      'default_value' => trim($data['value']),
    ))
    ->execute();

  //was merge query successful?
  if ($merge) {
    _custom_keywords_clear_statics(array('custom_keywords_load_keywords'));
    return $merge;
  }


  return FALSE;
}

/**
 * delete keywords from the database
 * @param  array $names keyword names or name of string
 * @return boolean         boolean true or false is successful
 */
function custom_keywords_delete_keywords($names = FALSE) {

  //build delete queries for keyword tables
  $delete_entity  = db_delete('custom_keywords_entity_values');
  $delete_set     = db_delete('custom_keywords_set_values');
  $delete_keyword = db_delete('custom_keywords');

  //only deleting specific keywords?
  if ($names) {
    if (!is_array($names)) {
      $names = array($names);
    }
    //add conditions to each query
    $delete_entity->condition('keyword', $names, 'IN');
    $delete_set->condition('keyword', $names, 'IN');
    $delete_keyword->condition('name', $names, 'IN');
  }

  //execute functions
  $delete_entity->execute();
  $delete_set->execute();
  $delete_keyword->execute();

  //empty statics?
  _custom_keywords_clear_statics();

  //see if keywords still exist
  $keywords = custom_keywords_load_keywords($names);
  if (count($keywords) > 0) {
    //failed to delete
    return FALSE;
  }

  return TRUE;
}

/**
 * Save a keyword set
 * @param  string $name set machine name
 * @param  array $data array of set data
 * @return boolean       TRUE or FALSE
 */
function custom_keywords_save_set($name, $data) {
  $default_data = array(
    'enabled' => 1,
  );

  //add default fields
  $data = array_merge($default_data, $data);

  //run merge query
  $merge = db_merge('custom_keywords_sets')
    ->key(array(
        'name'  => $name,
      ))
    ->fields(array(
        'enabled' => $data['enabled'] ? 1:0,
      ))
    ->execute();

  if ($merge) {
      return $merge;
  }

  return FALSE;
}

/**
 * delete sets from database
 * @param  array $names name(s) of specific sets
 * @return boolean         boolean TRUE or FALSE
 */
function custom_keywords_delete_sets($names = FALSE) {
  $delete_values  = db_delete('custom_keywords_set_values');
  $delete_set     = db_delete('custom_keywords_sets');

  //only deleting specific sets?
  if ($names) {
    if (!is_array($names)) {
      $names = array($names);
    }
    //add conditions to each query
    $delete_values->condition('set_name', $names, 'IN');
    $delete_set->condition('name', $names, 'IN');
  }

  //execute functions
  $delete_values->execute();
  $delete_set->execute();

  //empty statics?
  _custom_keywords_clear_statics();
  //see if sets still exist
  if (count(custom_keywords_get_sets($names))>0) {
    //failed to delete
    return FALSE;
  }

  return TRUE;
}

/**
 * save keywords for a set.
 * @param  string $name     machine name of set
 * @param  array $keywords array of keyword data
 * @return boolean           TRUE or FALSE
 */
function custom_keywords_save_set_keywords($name, $keywords) {

  $insert_query = db_insert('custom_keywords_set_values')
    ->fields(array('set_name', 'keyword', 'custom_value'));

  foreach ($keywords as $keyword => $value) {
    $insert_query->values(array(
      'set_name'           => $name,
      'keyword'       => trim($keyword),
      'custom_value'  => trim($value)
    ));
  }

  //delete all old set keywords
  db_delete('custom_keywords_set_values')
    ->condition('set_name', $name)
    ->execute();

  //insert new values
  $insert_query->execute();

  //clear statics
  _custom_keywords_clear_statics();

  //make sure insert worked
  $set_values = custom_keywords_get_set_values($name);

  if (count($set_values) == count($keywords)) {
    return TRUE;
  }

  return FALSE;
}

/**
 * save keywords for a specific entity
 * @param  array  $keywords array of keywords
 * @param  string  $type     entity type
 * @param string $bundle the bundle type
 * @param  integer  $id       entity id
 * @param  integer $vid      revision id
 * @param  string  $language entity language
 * @return none
 */
function custom_keywords_save_entity_keywords($entity, $type) {
  // If there are no keywords then just return.
  if (empty($entity->custom_keywords)) {
    return;
  }
  // Grab the keywords that were passed along during the save.
  $keywords = $entity->custom_keywords;

  // Get the entitie's language.
  $language = custom_keywords_entity_get_language($type, $entity);

  // Get id, new revision (if revisioning), and the bundle type of the entity.
  list($id, $vid, $bundle) = entity_extract_ids($type, $entity);

  // Some entities are not revisioned
  if(!is_numeric($vid)){
    $vid = 1;
  }

  // Get default/parent values.
  $default_values = array();

  // Load the default, translated values.
  custom_keywords_load_entity_keyword_defaults($default_values, $type, $bundle);

  if (!empty($entity->is_new)) {
    // If the entity is new, we just need to store any custom values that were
    // set if they are different from inherited values.

    $insert_query = db_insert('custom_keywords_entity_values')
      ->fields(array('type', 'eid', 'vid', 'language', 'keyword', 'custom_value'));
    foreach ($default_values as $keyword => $value) {

      // Sometimes values are stored in an array
      if(is_array($keywords[$keyword])){
        $custom_value = $keywords[$keyword]['value'];
      }else{
        $custom_value = $keywords[$keyword];
      }

      $default_value = $value['value'];

      if ($default_value !== $custom_value) {
        // If a keyword is empty, do nothing.
        if (empty($keywords[$keyword])) {
          continue;
        }
        $insert_query->values(array(
          'type'         => $type,
          'eid'          => $id,
          'vid'          => $vid,
          'language'     => $language,
          'keyword'      => $keyword,
          'custom_value' => $custom_value,
        ));
      }
    }
    // Finally, add the appropriate entries.
    $insert_query->execute();
  }
  else {
    if (!empty($entity->is_new_revision) || !empty($entity->revision)) {
      $revision_list = _get_entity_revision_list($type, $entity);
      $current_vid = end($revision_list);
      $old_vid = prev($revision_list);
      // If the entity is not new, but is a new revision, then we need to take
      // any keywords that are different from the default, and insert new
      // entries for each of those keywords. Additionally, any keywords stored
      // in a different language in the last revision must have new entries
      // inserted for each in the current revision.
      $insert_query = db_insert('custom_keywords_entity_values')
        ->fields(array('type', 'eid', 'vid', 'language', 'keyword', 'custom_value'));
      foreach ($default_values as $keyword => $value) {

        // Sometimes values are stored in an array
        if(is_array($keywords[$keyword])){
          $custom_value = $keywords[$keyword]['value'];
        }else{
          $custom_value = $keywords[$keyword];
        }

        $default_value = $value['value'];

        if ($default_value !== $custom_value) {
          // If the new keyword value is empty, that means the user wants
          // to revert it's value to whatever the set or global value is, so
          // we simply don't insert an entry for this keyword.
          if (empty($keywords[$keyword]['value'])) {
            continue;
          }
          $insert_query->values(array(
            'type'         => $type,
            'eid'          => $id,
            'vid'          => $vid,
            'language'     => $language,
            'keyword'      => $keyword,
            'custom_value' => $custom_value,
          ));
        }
      }
      // Finally, add the appropriate entries.
      $insert_query->execute();
      $language_update_rows = db_select('custom_keywords_entity_values', 'ck')
        ->fields('ck')
        ->condition('language', $language, '<>')
        ->condition('eid', $id)
        ->condition('type', $type)
        ->condition('vid', $old_vid)
        ->execute()
        ->fetchAll();
      $language_insert = db_insert('custom_keywords_entity_values')
        ->fields(array('type', 'eid', 'vid', 'language', 'keyword', 'custom_value'));
      foreach ($language_update_rows as $row) {
        $language_insert->values(array(
          'type'         => $type,
          'eid'          => $id,
          'vid'          => $vid,
          'language'     => $row->language,
          'keyword'      => $keyword,
          'custom_value' => $row->custom_value,
        ));
      }
      $language_insert->execute();
    }
    else {
      // If the entity is not new and we're not creating a new revision, then we
      // need to update the values that are currently stored if any of those
      // values are different from what is present in $keywords.
      $insert_query = db_insert('custom_keywords_entity_values')
        ->fields(array('type', 'eid', 'vid', 'language', 'keyword', 'custom_value'));

      $delete_query = db_delete('custom_keywords_entity_values')
        ->condition('type', $type)
        ->condition('eid', $id)
        ->condition('vid', $vid)
        ->condition('language', $language);

      $deleted_keywords = array();

      foreach ($default_values as $keyword => $value) {
        $default_value = $value['value'];
        if ($default_value !== $keywords[$keyword]['value']) {
          $deleted_keywords[] = $keyword;
          // If the new keyword value is empty, that means the user wants
          // to revert it's value to whatever the set or global value is, so
          // we simply just delete this instance of the entity keyword and
          // move on - there is no need to insert anything.
          if (empty($keywords[$keyword])) {
            continue;
          }
          $insert_query->values(array(
            'type'         => $type,
            'eid'          => $id,
            'vid'          => $vid,
            'language'     => $language,
            'keyword'      => $keyword,
            'custom_value' => $keywords[$keyword]['value'],
          ));
        }
      }
      // If there is anything to delete, then delete them.
      if (!empty($deleted_keywords)) {
        $delete_query->condition('keyword', $deleted_keywords)
          ->execute();
      }
      // Finally, add the appropriate entries.
      $insert_query->execute();
    }
  }
}

/**
 * load custom keyword and default values
 * @param  boolean $names names of specific keywords to fetch
 * @return array         array of keywords
 */
function custom_keywords_load_keywords($names = FALSE) {
  $keywords = &drupal_static(__FUNCTION__);
  //have not previously retrieve the keywords?
  if (!$keywords) {
    //@TODO: add caching
    //fetch from db
    $query = db_select('custom_keywords', 'k')
      ->fields('k', array('kid', 'name', 'description'));
    $query->addField('k', 'default_value', 'value');
    $keywords = $query->execute()
      ->fetchAllAssoc('name', PDO::FETCH_ASSOC);
    foreach ($keywords as $key_id => &$keyword) {
      $obj_name = "custom_keywords:global:$key_id:value";
      $keyword['value'] = custom_keywords_translate($obj_name, $keyword['value']);
    }
  }

  //only want specific keywords?
  if ($names) {
    if (is_string($names)) {
      $names = array($names);
    }
    //return the intersection
    return array_intersect_key($keywords, array_flip($names));
  }


  return $keywords;
}

/**
 * load keywords for a specifi set
 * @param  string  $name         the set machine name
 * @param  boolean $enabled_only flag to only return enabled set
 * @return array                array of keywords and values
 */
function custom_keywords_load_set_keywords($name, $enabled_only = FALSE) {
  //if dont care is set is enabled, or we do and it is.
  if (!$enabled_only || ($enabled_only && _custom_keywords_set_enabled($name))) {
   //get the specific keyword values for this set or its parents
    if ($set_values = custom_keywords_get_set_values($name, TRUE)) {
      //do nothing
    }
    else {
      $set_values = array();
    }
  }
  else {
    $set_values = array();
  }

  // Initialize global defaults.
  $keywords = custom_keywords_load_keywords();
  // Replace global values with specific values.
  $set_keywords = array_replace_recursive($keywords, $set_values);

  return $set_keywords;
}

/**
 * load keywords for a specific entity
 * @param  string   $type   the entity type
 * @param  string   $bundle the entity bundle
 * @param  integer  $eid    the entity id
 * @param  integer  $vid    the entity revision id
 * @param string    $language the entity language
 * @return array         array of keywords and values
 */
function custom_keywords_load_entity_keywords($type, $bundle = '', $eid, $vid = 1, $language = LANGUAGE_NONE) {
  if (!is_numeric($vid)) {
    $vid = 1;
  }

  $entity_keywords = array();

  //get the sepecific keyword values for this entity
  if ($entity_values = custom_keywords_get_entity_values($type, $eid, $vid, $language)) {
    //retireve the global keywords
    $keywords = custom_keywords_load_keywords();
    //find intercetion of entity keywords and all keywords
    $intersect = array_intersect_assoc($keywords, $entity_values);
    //add keyword structures for just the entity keywords
    $entity_keywords = array_replace_recursive($intersect, $entity_values);
  }
  else {
    //set as an empty array
    $entity_keywords = array();
  }


  return $entity_keywords;
}

/**
 * load default values and missing keywords from parents/global
 * @param  array &$entity_keywords keywords for particualr entity
 * @param  string $type            entity type
 * @param  string $bundle          entity bundle
 * @return none
 */
function custom_keywords_load_entity_keyword_defaults(&$entity_keywords, $type, $bundle = ''){
  //figure out parent set
  $parent_set = $type . ($bundle ? ':' . $bundle : '');

  //update values from parent set recursivley
  if ($set_values = custom_keywords_get_set_values($parent_set, TRUE)) {
    //replace values
    $entity_keywords = array_replace_recursive($set_values, $entity_keywords);
  }

  //retrieve the global keywords
  $keywords = custom_keywords_load_keywords();

  //replace global values with specific values
  $entity_keywords = array_replace_recursive($keywords, $entity_keywords);
}

/**
 * retrieve sets from database
 * @param  string $names name of sets to retrieve
 * @return array         array of sets
 */
function custom_keywords_get_sets($names = FALSE) {
  $sets = &drupal_static(__FUNCTION__);

  //previously retrieved sets?
  if (!$sets) {
    //@TODO: add caching
    //fetch from db
    $sets = db_select('custom_keywords_sets', 's')
      ->fields('s')
      ->execute()
      ->fetchAllAssoc('name', PDO::FETCH_ASSOC);
  }

  //only want particular sets? filter the sets array
  if ($names) {
    if (is_string($names)) {
      $names = array($names);
    }

    return array_intersect_key($sets, array_flip($names));
  }

  return $sets;
}

/**
 * retrieve a nested array of sets
 * @param  array $names names of sets to retrieve or FALSE for all
 * @return array         array of sets in a nested array
 */
function custom_keywords_get_nested_sets($names = FALSE) {
  //able to retrieve sets?
  if ($sets  = custom_keywords_get_sets($names)) {
    $nested = array();

    //loop through each set to build nested array
    foreach ($sets as $name => $set) {
      //build ordered list of parents
      $parents = array();
      $parent = $name;
      //loop while ':' is present in parent name
      while (strpos($parent, ':')) {
        $parent = substr($parent, 0, strrpos($parent, ':'));
        //add to begining of parents array
        array_unshift($parents, $parent);
      }

      //add to nested array in correct place, if have parents
      if (count($parents) > 0) {
        $location = &$nested;

        while (count($parents) > 0) {
          //chop off parents and set chold location
          $location = &$location[array_shift($parents)]['children'];
        }
      }
      else{
        $location = &$nested;
      }
      //add to proper location in nested array
      $location[$name] = $set;
    }

    return $nested;
  }
  return FALSE;
}

/**
 * retrieve the keyword values for a particular set
 * @param  string $name machine name of set
 * @return array       keyword values for this set
 */
function custom_keywords_get_set_values($name, $recursive = FALSE) {
  $set_values = &drupal_static(__FUNCTION__);

  if (!$set_values) {
    $set_values = array();
  }

  //set not retireved yet?
  if (!isset($set_values[$name])) {
    $query = db_select('custom_keywords_set_values', 'sv');
    $query->addField('sv', 'keyword', 'name');
    $query->addField('sv', 'custom_value', 'value');
    $keywords = $query->condition('sv.set_name', $name)
      ->execute()
      ->fetchAllAssoc('name', PDO::FETCH_ASSOC);
    foreach ($keywords as $key_id => &$keyword) {
      $objid = str_replace(':', '-', $name);
      $obj_name = "custom_keywords:set:$objid|$key_id:value";
      $keyword['value'] = custom_keywords_translate($obj_name, $keyword['value']);
    }
    $set_values[$name] = $keywords;
  }

  $set = $set_values[$name];

  if ($recursive && strpos($name, ':')) {
    //retrieve values from parent set
    $parent_set = substr($name, 0, strrpos($name, ':'));

    if ($parent_values = custom_keywords_get_set_values($parent_set, TRUE)) {
      if (!$set) {
        $set = array();
      }

      $set = array_replace_recursive($parent_values, $set);
    }
  }

  return $set;
}

/**
 * get keyword values for a particular entity
 * @param  string  $type     entity type
 * @param  integer  $eid      entity id
 * @param  integer $vid      entity revision id
 * @param  string  $language entity language
 * @return array            array of keyword and values
 */
function custom_keywords_get_entity_values($type, $eid, $vid = 1, $language = LANGUAGE_NONE) {
  $entity_values = &drupal_static(__FUNCTION__);


  if (!is_numeric($vid)) {
    $vid = 1;
  }

  // Not previouly retrieved entities?
  if (!$entity_values) {
    $entity_values = array();
  }

  // Not already retrieve keywords for this entity?
  if (!isset($entity_values[$type][$eid][$vid][$language])) {
    // Fetch keywords from db.
    $query = db_select('custom_keywords_entity_values', 'ev');
    $query->addField('ev', 'keyword', 'name');
    $query->addField('ev', 'custom_value', 'value');
    $keywords = $query->condition('type', $type)
      ->condition('ev.eid', $eid)
      ->condition('ev.vid', $vid)
      ->condition('ev.language', $language)
      ->execute()
      ->fetchAllAssoc('name', PDO::FETCH_ASSOC);
    // Add to entity values array.
    $entity_values[$type][$eid][$vid][$language] = $keywords;
  }

  //return keywords for this entity (or false)
  return $entity_values[$type][$eid][$vid][$language];
}

/**
 * boolean check is a set is enabled
 * @param  string $name set machine name
 * @return boolean       boolean true or false
 */
function _custom_keywords_set_enabled($name) {
  return db_select('custom_keywords_sets', 's')
    ->fields('s', array('name'))
    ->condition('s.name', $name)
    ->condition('s.enabled', 1)
    ->execute()
    ->rowCount();
}

/**
 * Wrapper around entity_language() to use LANGUAGE_NONE if the entity does not
 * have a language assigned.
 *
 * @param $type
 *   An entity type's machine name.
 * @param $entity
 *   The entity to review;
 *
 * @return
 *   A string indicating the language code to be used.
 */
function custom_keywords_entity_get_language($type, $entity) {
  // Determine the entity's language.
  $langcode = entity_language($type, $entity);

  // If no matching language was found, which will happen for e.g., terms and
  // users, it is normally recommended to use the system default language.
  // However, as the system default language can change, this could potentially
  // cause data loss / confusion problems; as a result use the system "no
  // language" value to avoid any potential problems.
  if (empty($langcode)) {
    $langcode = LANGUAGE_NONE;
  }

  return $langcode;
}

/**
 * keeps sets up to date with available entity types and bundles.
 * @return none
 */
function custom_keywords_cleanup_sets() {
  $insert_sets = array();
  $delete_sets = array();
  //get all existing sets
  if ($existing_sets = custom_keywords_get_sets()) {
    //only want names
    $existing_sets = array_keys($existing_sets);
  }
  else {
    $existing_sets = array();
  }

  //get all entity types with bundles
  $entity_types = entity_get_info();

  foreach ($entity_types as $type => $info) {
    //fieldable?
    if (!$info['fieldable']) {
      continue;
    }

    //type already exist as a set?
    if (!in_array($type, $existing_sets)) {
      //add to sets
      $insert_sets[] = $type;
    }

    //check all bundles
    foreach ($info['bundles'] as $bundle => $data) {
      if ($bundle == $type) {
        continue;
      }
      $set_name = $type . ':' . $bundle;
      if (!in_array($type, $existing_sets)) {
        //add to sets
        $insert_sets[] = $set_name;
      }
    }
  }

  //figure out old sets to delete
  foreach ($existing_sets as $name) {
    if (strpos($name, ':')) {
      $name = explode(':', $name);
      $type = $name[0];
      $bundle = $name[1];

      //see if entity tye still exists
      if (!isset($entity_types[$type]) && !in_array($type, $delete_sets)) {
        $delete_sets[] = $type;
      }

      //see if bundle set exists
      if (!isset($entity_types[$type]['bundles'][$bundle])) {
        $delete_sets[] = $type . $bundle;
      }
    }
    elseif (!isset($entity_types[$name]) && !in_array($name, $delete_sets)) {
      $delete_sets[] = $name;
    }
  }

  //have sets to delete?
  if (count($delete_sets) > 0) {
    custom_keywords_delete_sets($delete_sets);
    //deleting sets here, means the entity type / bundle has been removed
    //delete entity values for any entities that are left for these
    //sets. Drupal should have handled this when the type/bundle was deleted
    //but just to be safe.
    db_delete('custom_keywords_entity_values')
      ->condition('type', $delete_sets, 'IN')
      ->execute();
  }

  //have sets to insert?
  if (count($insert_sets) > 0) {
    //save them
    foreach ($insert_sets as $set) {
      custom_keywords_save_set($set, array());
    }
  }
}

/**
 * Custom keyword loader function.  Used in page paths.  Returns an array if
 * the keyword was found, or NULL otherwise.
 * @param  string $keyword
 * @return array
 */
function custom_keywords_load($keyword) {
  $keywords = &drupal_static(__FUNCTION__);

  if ($keywords) {
    return $keywords[$keyword];
  }

  $keywords = custom_keywords_load_keywords(array($keyword));

  if ($keywords) {
    return $keywords[$keyword];
  }
  else {
    return NULL;
  }
}

/**
 * Clear static caches set during the current page request.
 * @param  boolean $names
 * @return none
 */
function _custom_keywords_clear_statics($names = FALSE) {
  if ($names && is_array($names)) {
    foreach ($names as $key => $value) {
      drupal_static_reset($value);
    }
  }
  else {
    drupal_static_reset('custom_keywords_load_keywords');
    drupal_static_reset('custom_keywords_load');
    drupal_static_reset('custom_keywords_get_sets');
    drupal_static_reset('custom_keywords_get_set_values');
    drupal_static_reset('custom_keywords_get_entity_values');
  }
}

/**
 * Tranlsate custom keywords if i18n_strings is enabled.
 */
function custom_keywords_translate($name, $string) {
  return function_exists('i18n_string') ? i18n_string($name, $string) : $string;
}

/**
 * Get a revision id list for a particular entity.
 * @todo  implement static caching using drupal_static()
 */
function _get_entity_revision_list($type, $entity) {
  $info = entity_get_info($type);
  if (!isset($info['revision table'])) {
    // If this entity does not track revisions then return FALSE.
    return FALSE;
  }
  if (!isset($info['entity keys']['revision'])) {
    // If for whatever reason a revision table was defined, but no revision key
    // then also return FALSE.
    return FALSE;
  }
  $revisions = db_select($info['revision table'], 'r')
    ->fields('r', array($info['entity keys']['revision']))
    ->condition($info['entity keys']['id'], $entity->{$info['entity keys']['id']})
    ->execute()
    ->fetchAllAssoc($info['entity keys']['revision']);
  return array_keys($revisions);
}
