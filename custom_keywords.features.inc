<?php
/**
 * @file
 * Features file for the custom_keywords module.
 */

/**
 * Implements hook_features_api().
 */
function custom_keywords_features_api() {
  return array(
    'custom_keywords' => array(
      'name'            => t('Custom Keywords'),
      'default_hook'    => 'custom_keywords_export_keywords',
      'feature_source'  => TRUE,
      'default_file'    => FEATURES_DEFAULTS_INCLUDED,
      'file'            => drupal_get_path('module', 'custom_keywords') . '/custom_keywords.features.inc',
    ),
    'custom_keywords_sets' => array(
      'name'            => t('Custom Keyword Sets'),
      'default_hook'    => 'custom_keywords_export_sets',
      'feature_source'  => TRUE,
      'default_file'    => FEATURES_DEFAULTS_INCLUDED,
      'file'            => drupal_get_path('module', 'custom_keywords') . '/custom_keywords.features.inc',
    ),
  );
}

/**
 * Implements COMPONENT_features_export_options().
 *
 * Inform features about the available custom keywords in the database.
 */
function custom_keywords_features_export_options() {
  return db_select('custom_keywords', 'k')
    ->fields('k', array('name', 'name'))
    ->execute()
    ->fetchAllKeyed();
}

/**
 * Implements COMPONENT_features_export_options().
 *
 * Inform features about the available custom keywords in the database.
 */
function custom_keywords_sets_features_export_options() {
  return db_select('custom_keywords_sets', 's')
    ->fields('s', array('name', 'name'))
    ->execute()
    ->fetchAllKeyed();
}

/**
 * Implements COMPONENT_features_export().
 *
 * Process the features export array for custom keywords.
 */
function custom_keywords_features_export($data, &$export, $module_name) {
  $export['dependencies']['custom_keywords'] = 'custom_keywords';

  foreach ($data as $component) {
    $export['features']['custom_keywords'][$component] = $component;
  }

  return array();
}

/**
 * Implements COMPONENT_features_export().
 *
 * Process the features export array for custom keywords.
 */
function custom_keywords_sets_features_export($data, &$export, $module_name) {
  $pipe = array('custom_keywords' => array());

  $export['dependencies']['custom_keywords'] = 'custom_keywords';

  foreach ($data as $component) {
    $export['features']['custom_keywords_sets'][$component] = $component;
    if ($keywords = custom_keywords_get_set_values($component)) {
      foreach ($keywords as $name => $value) {
        $pipe['custom_keywords'][$name] = $name;
      }
    }
  }

  return $pipe;
}

/**
 * Implements COMPONENT_features_export_render().
 *
 * Render workbench custom keywords as code.
 */
function custom_keywords_features_export_render($module_name, $data) {
  $items = array();
  $items = custom_keywords_load_keywords($data);
  foreach ($items as &$keyword) {
    unset($keyword['kid']);
  }
  $code = "  \$items = " . features_var_export($items, '  ') . ";\n";
  $code .= '  return $items;';

  return array('custom_keywords_export_keywords' => $code);
}

/**
 * Implements COMPONENT_features_export_render().
 *
 * Render workbench custom keywords as code.
 */
function custom_keywords_sets_features_export_render($module_name, $data) {
  $items = array();
  $items = custom_keywords_get_sets($data);
  //foreach set load keywords
  foreach ($items as $name => &$set) {
    unset($set['sid']);
    $set['keywords'] = custom_keywords_get_set_values($name);
  }

  $code = "  \$items = " . features_var_export($items, '  ') . ";\n";
  $code .= '  return $items;';

  return array('custom_keywords_export_sets' => $code);
}

/**
 * Implements COMPONENT_features_revert().
 */
function custom_keywords_features_revert($module) {
  custom_keywords_features_rebuild($module);
}

/**
 * Implements COMPONENT_features_enable_feature().
 */
function custom_keywords_sets_features_enable_feature($module) {
  custom_keywords_sets_features_rebuild($module);
}

/**
 * Implements COMPONENT_features_rebuild().
 *
 * Store each exported schedule in the database.
 */
function custom_keywords_features_rebuild($module) {
  $defaults = features_get_default('custom_keywords', $module);
  foreach ($defaults as $name => $keyword) {
    custom_keywords_save_keyword($name, $keyword);
  }
  drupal_static_reset('custom_keywords_load_keywords');
}

/**
 * Implements COMPONENT_features_rebuild().
 *
 * Store each exported schedule in the database.
 */
function custom_keywords_sets_features_rebuild($module) {
  $defaults = features_get_default('custom_keywords_sets', $module);
  foreach ($defaults as $name => $set) {
    $keywords = $set['keywords'];
    unset($set['keywords']);
    custom_keywords_save_set($name, $set);
    custom_keywords_save_set_keywords($name, $keywords);
  }
  drupal_static_reset('custom_keywords_load_sets');
}
