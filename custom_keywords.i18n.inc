<?php
/**
 * @file
 * Internationalization (i18n) hooks.
 */

/**
 * Implements hook_i18n_string_info().
 */
function custom_keywords_i18n_string_info() {
  $groups['custom_keywords'] = array(
    'title' => t('Custom Keywords'),
    'description' => t('Configurable custom keywords.'),
    'format' => FALSE,
    'list' => TRUE,
  );
  return $groups;
}

/**
 * Implements hook_i18n_string_list().
 */
function custom_keywords_i18n_string_list($group) {
  if ($group == 'custom_keywords') {
    $strings = array();
    $global = &$strings['custom_keywords']['global'];
    $set_keywords = &$strings['custom_keywords']['set'];
    $entity_keywords = &$strings['custom_keywords']['entity'];

    foreach(_custom_keywords_get_global_keywords() as $keyword => $object) {
      $global[$keyword]['value'] = $object->default_value;
    }
    foreach (_custom_keywords_get_set_keywords() as $index => $object) {
      $set_name = str_replace(':', '-', $object->set_name);
      $set_keywords["$set_name|{$object->keyword}"]['value'] = $object->custom_value;
    }

    return $strings;
  }
}

/**
 * Fetch all global keywords.
 * @return array keyed by keyword with attached stdClass table entry
 */
function _custom_keywords_get_global_keywords() {
  $query = &drupal_static(__FUNCTION__);
  if ($query) {
    return $query;
  }
  $query = db_select('custom_keywords', 'ck')
    ->fields('ck')
    ->execute()
    ->fetchAllAssoc('name');
  return $query;
}

function _custom_keywords_get_set_keywords() {
  $query = &drupal_static(__FUNCTION__);
  if ($query) {
    return $query;
  }
  $query = db_select('custom_keywords_set_values', 'ck')
    ->fields('ck')
    ->execute()
    ->fetchAll();
  return $query;
}
